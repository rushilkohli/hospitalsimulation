package javaapplication23;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rushil
 */
public class HospitalSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Doctor doctor = new Doctor("Dr. Rushil");
        Barcode code = new Barcode("abcdef1234");
        Medication meds = new Medication("Advil");
        List<Medication> med = new ArrayList<Medication>();
        med.add(meds);
        String patientName = "John Smith";
        WristBand band = new AllergyWristBand(patientName, doctor, new Date(), code, med);
        List<WristBand> bands = new ArrayList<WristBand>();
        bands.add(band);
        
        Patient patient = new Patient(patientName, bands);
        List<Patient> patients = new ArrayList<Patient>();
        patients.add(patient);
        ResearchGroup group = new ResearchGroup(patients);
        
        System.out.println(med.toString());
        System.out.println(band.toString());
        System.out.println(patients.toString());
    }
    
}
