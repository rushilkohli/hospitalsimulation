package javaapplication23;

import java.util.Date;
import java.util.List;

/**
 *
 * @author rushil
 */
public class AllergyWristBand extends WristBand {
    List<Medication> medication;

    AllergyWristBand(String patientName, Doctor doc, Date dob, Barcode code, List<Medication> medication) {
        super(patientName, doc, dob, code);
        this.medication = medication;
    }
}
