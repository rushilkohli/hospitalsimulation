package javaapplication23;

import java.util.Date;
import java.util.List;

/**
 *
 * @author rushil
 */
public class ChildWristBand extends WristBand {
    private List<Parent> parents;

    public ChildWristBand(String name, Doctor doc, Date dob, Barcode barcode, List<Parent> parents) {
        super(name, doc, dob, barcode);
        this.parents = parents;
    }
}