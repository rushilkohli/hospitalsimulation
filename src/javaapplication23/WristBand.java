package javaapplication23;

import java.util.Date;

/**
 *
 * @author rushil
 */
public class WristBand {
    private String name;
    private Doctor doc;
    private Date dob;
    private Barcode barcode;
    public WristBand() {
        
    }
    
    public WristBand(String name, Doctor doc, Date dob, Barcode barcode) {
        this.name = name;
        this.doc = doc;
        this.dob =dob;
        this.barcode = barcode;
    }
    
    @Override
    public String toString() {
        return "Patient Name: " + name + "\n" + "Doctor: " + doc + "\n" + "Date: " + dob + "\n" + "Barcode: " +barcode;
    }
}





