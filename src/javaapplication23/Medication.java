package javaapplication23;

/**
 *
 * @author rushil
 */
public class Medication {
    private String medicine;
    public Medication() {
        
    }
    
    public Medication(String medicine) {
        this.medicine = medicine;
    }
    
    @Override
    public String toString() {
        return "Medicine: " + medicine;
    }
}
