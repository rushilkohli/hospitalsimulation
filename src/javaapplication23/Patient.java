package javaapplication23;

import java.util.List;

/**
 *
 * @author rushil
 */
public class Patient extends Person {
    List<WristBand> bands;
    
    public Patient(String name, List<WristBand> bands) {
        super(name);
    }
    
    @Override
    public String toString() {
        return "Bands: " + bands;
    }
}
