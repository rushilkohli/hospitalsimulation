package javaapplication23;

/**
 *
 * @author rushil
 */
public class Person {
    private String name;
    
    protected Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
